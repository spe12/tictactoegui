/*

Created by Shaun Enriquez

enriquezshaun94@aol.com

Finished on 2/27/16

Sources:
1)https://cs.fit.edu/~mmahoney/cis5200/TicTacToe.java
--- Used checking of conditions (winning/losing), playing against computer creating grid using the geometry
2)http://www.javacodegeeks.com/2012/02/java-swing-tic-tac-toe.html
--- Used similar code hierarchy , two-player game structure

 */

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.Random;

public class TicTacToe extends JFrame
{
	
	Board board;
	int lineThickness = 2;
	Color xColor = Color.decode("#DB0000"); // using Color.decode to set hex values for color
	Color oColor = Color.decode("#0000ff"); // using Color.decode to set hex values for color
	final char BLANK=' ', O='O', X='X';
	int turnCounter = 0;
	
	//position on the board
	char position[]={
			BLANK, BLANK, BLANK,
			BLANK, BLANK, BLANK,
			BLANK, BLANK, BLANK};
	
	//default option is two player
	boolean againstComputer;

  // main method - starts game
  public static void main(String args[])
  {
    new TicTacToe();
  }

  // constructor
  public TicTacToe()
  { 
	  // window title
	  super("Tic Tac Toe");
		  
		  if (JOptionPane.showConfirmDialog(null, 
		          "Please click 'yes' to play against another person. Please click 'no' to play against the computer", "Tic-Tac-Toe Game Settings", JOptionPane.YES_NO_OPTION)
		          !=JOptionPane.YES_OPTION)
		  {
			  againstComputer = true;
		  }
		  else
			  againstComputer = false;
	  
	  // title text
	  JLabel toptitle = new JLabel("FSU", JLabel.CENTER);
	  toptitle.setFont(new Font("Serif", Font.BOLD, 48));
	  toptitle.setForeground(Color.decode("#F0E90C"));
    
	  // creating top panel - fsu
	  JPanel topPanel=new JPanel();
	  topPanel.setLayout(new FlowLayout());
	  topPanel.add(toptitle);
	  topPanel.setBackground(Color.decode("#DB0000"));
	  add(topPanel, BorderLayout.NORTH);
    
	  // creating tic tac toe board in center
	  add(board=new Board(), BorderLayout.CENTER);
	  
	  // title text
	  JLabel bottomTitle = new JLabel("Created by: Shaun Enriquez", JLabel.CENTER);
	  bottomTitle.setFont(new Font("Serif", Font.BOLD, 24));
	  bottomTitle.setForeground(Color.decode("#F0E90C"));
    
	  // creating bottom panel - created by
	  JPanel bottomPanel=new JPanel();
	  bottomPanel.setLayout(new FlowLayout());
	  bottomPanel.add(bottomTitle);
	  bottomPanel.setBackground(Color.decode("#DB0000")); // using Color.decode to set hex values for color
	  add(bottomPanel, BorderLayout.SOUTH);
    
	  // exit on close - size - visibility
	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  setSize(500, 600);
	  setVisible(true);
  }

  

  	// Board is what actually plays and displays the game
  class Board extends JPanel implements MouseListener
  {
    private Random random=new Random();
    private int rows[][]={{0,2},{3,5},{6,8},{0,6},{1,7},{2,8},{0,8},{2,6}};
      // Endpoints of the 8 rows in position[] (across, down, diagonally)
    
    //register listener
    public Board()
    {
      addMouseListener(this);
    }

    // Redraw the board
    public void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      int w=getWidth();
      int h=getHeight();
      
      //graphic output to the screen
      Graphics2D graphicOutput = (Graphics2D) g;

      // draw board
      graphicOutput.setPaint(Color.WHITE);
      graphicOutput.fill(new Rectangle2D.Double(0, 0, w, h));
      graphicOutput.setPaint(Color.BLACK);
      graphicOutput.setStroke(new BasicStroke(lineThickness));
      graphicOutput.draw(new Line2D.Double(0, h/3, w, h/3));
      graphicOutput.draw(new Line2D.Double(0, h*2/3, w, h*2/3));
      graphicOutput.draw(new Line2D.Double(w/3, 0, w/3, h));
      graphicOutput.draw(new Line2D.Double(w*2/3, 0, w*2/3, h));

      // drawing X or O
      for (int i=0; i<9; ++i)
      {
	        double xpos=(i%3+0.5)*w/3.0;
	        double ypos=(i/3+0.5)*h/3.0;
	        double xr=w/8.0;
	        double yr=h/8.0;
	        
	        if (position[i]==O)
	        {
	        	graphicOutput.setPaint(oColor);
	        	graphicOutput.draw(new Ellipse2D.Double(xpos-xr, ypos-yr, xr*2, yr*2));
	        }
	        else if (position[i]==X) {
	        	graphicOutput.setPaint(xColor);
	        	graphicOutput.draw(new Line2D.Double(xpos-xr, ypos-yr, xpos+xr, ypos+yr));
	        	graphicOutput.draw(new Line2D.Double(xpos-xr, ypos+yr, xpos+xr, ypos-yr));
	        }
      }
      
     }

    // shows what happens on mouse clicks
    public void mouseClicked(MouseEvent e)
    {
      int xpos=e.getX()*3/getWidth();
      int ypos=e.getY()*3/getHeight();
      int pos=xpos+3*ypos;
      
      // if O's turn and NOT playing against computer
      if( (turnCounter%2==0) && (againstComputer==false))
      {
    	  if (pos>=0 && pos<9 && position[pos]==BLANK)
          {
            position[pos]=O;
            turnCounter++;
            repaint();
            twoPlayerCheckO();
          }
      }
      // if X's turn and NOT playing against computer
      else if( (turnCounter%2!=0) && (againstComputer==false))
      {
    	  if (pos>=0 && pos<9 && position[pos]==BLANK)
          {
            position[pos]=X;
            turnCounter++;
            repaint();
            twoPlayerCheckX();
          }
      }
      // if playing against computer
      else
      {
    	  if (pos>=0 && pos<9 && position[pos]==BLANK)
          {
            position[pos]=O;
            repaint();
          }
    	  
    	  putX();  // computer plays
    	  repaint();
      }      
      
    }

    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}

    // computer moves
    void putX()
    {
      
      // Check if game is over
      if (won(O))
      {
        newGame(O);
      }
      else if (isDraw())
      {
        newGame(BLANK);
      }

      // play X
      else {
        nextMove();
        
        if (won(X)){
          newGame(X);
        }
        else if (isDraw())
        {
          newGame(BLANK);
        }
      }
    }
    
    // check if O wins in two player tic tac toe
    void twoPlayerCheckO()
    {
      // check if game is over
      if (won(O))
      {
        newGame(O);
      }
      else if (isDraw())
      {
        newGame(BLANK);
      }
    }
    
 // check if X wins in two player tic tac toe
    void twoPlayerCheckX()
    {
    	// check if game is over
        if (won(X))
        {
          newGame(X);
        }
        else if (isDraw())
        {
          newGame(BLANK);
        }
        
    }

    // checks to see if player has won
    // uses testRow as well
    boolean won(char player)
    {
      for (int i=0; i<8; ++i)
        if (testRow(player, rows[i][0], rows[i][1]))
          return true;
      return false;
    }

    boolean testRow(char player, int a, int b)
    {
      return position[a]==player && position[b]==player 
          && position[(a+b)/2]==player;
    }

    // computer finds best spot to play
    void nextMove()
    {
      int r=findRow(X);
      if (r<0)
        r=findRow(O);
      if (r<0) {
        do
          r=random.nextInt(9);
        while (position[r]!=BLANK);
      }
      position[r]=X;
    }

    // finding if there are two in a row
    int findRow(char player)
    {
      for (int i=0; i<8; ++i) {
        int result=find1Row(player, rows[i][0], rows[i][1]);
        if (result>=0)
          return result;
      }
      return -1;
    }
    
    // returning position of third in a row
    int find1Row(char player, int a, int b) {
      int c=(a+b)/2;
      if (position[a]==player && position[b]==player && position[c]==BLANK)
        return c;
      if (position[a]==player && position[c]==player && position[b]==BLANK)
        return b;
      if (position[b]==player && position[c]==player && position[a]==BLANK)
        return a;
      return -1;
    }

    // if all spots are filled with no winner - draw
    boolean isDraw()
    {
      for (int i=0; i<9; ++i)
        if (position[i]==BLANK)
          return false;
      return true;
    }

    // display winning message and see if you user wants to play again
    void newGame(char winner) {
      
    	if (winner=='O')
    	{
    		if (JOptionPane.showConfirmDialog(null, 
  		          "O is the winner!!!\nPlease click 'yes' to play again. \nPlease click 'no' to exit.", "Restart?", JOptionPane.YES_NO_OPTION)
  		          !=JOptionPane.YES_OPTION)
  		        System.exit(0);
    	}
    	else if (winner=='X')
    	{
    		if (JOptionPane.showConfirmDialog(null, 
  		          "X is the winner!!!\nPlease click 'yes' to play again. \nPlease click 'no' to exit.", "Restart?", JOptionPane.YES_NO_OPTION)
  		          !=JOptionPane.YES_OPTION)
  		        System.exit(0);
    	}
    	else
    	{
    		if (JOptionPane.showConfirmDialog(null, 
  		          "The game is a draw!\nPlease click 'yes' to play again. \nPlease click 'no' to exit.", "Restart?", JOptionPane.YES_NO_OPTION)
  		          !=JOptionPane.YES_OPTION)
  		        System.exit(0);
    	}

      for (int j=0; j<9; ++j)
        position[j]=BLANK;
      
      repaint();

    }
  }
}